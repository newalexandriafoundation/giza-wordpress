<?php
/**
 * @package WordPress
 * @subpackage CHS
 *
 * Register necessary custom post types
 *
 */

add_action( 'init', 'create_post_type' );
function create_post_type() {

  $is_blog = get_option('is_blog');
  $is_main_site = get_option('is_main_site');
  $is_milmanparry = get_option('is_milmanparry');
  $is_newalexandria = get_option('is_newalexandria');
  $is_ilexfoundation = get_option('is_ilexfoundation');

  if ($is_main_site || $is_milmanparry || $is_ilexfoundation) {
    register_post_type( 'book',
      array(
        'labels' => array(
          'name' => __( 'Books' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Book',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'chapter',
      array(
        'labels' => array(
          'name' => __( 'Chapters' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Chapter',
      'map_meta_cap' => true,
      'taxonomies' => array(),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'primary-source',
      array(
        'labels' => array(
          'name' => __( 'Primary Sources' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Primary Sources',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
  }
  if ($is_main_site) {
    register_post_type( 'curated-article',
      array(
        'labels' => array(
          'name' => __( 'Curated Articles' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Curated Article',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'feed-item',
      array(
        'labels' => array(
          'name' => __( 'Feed Item' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Feed Item',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', ),
      )
    );
    register_post_type( 'event',
      array(
        'labels' => array(
          'name' => __( 'Events' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Events',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
  }
  if ($is_milmanparry) {
    register_post_type( 'item',
      array(
        'labels' => array(
          'name' => __( 'Gallery Item' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Item such as a photograph or video from the collection Gallery (not the collections with Harvard Library)',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );

    register_post_type( 'collection',
      array(
        'labels' => array(
          'name' => __( 'Collection' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', ),
      'public' => true,
      'has_archive' => true,
      'description' => 'A collection to link to from the homepage',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'song',
      array(
        'labels' => array(
          'name' => __( 'Song' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', ),
      'public' => true,
      'has_archive' => true,
      'description' => 'A song in the collection (may be transcribed)',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'transcription',
      array(
        'labels' => array(
          'name' => __( 'Transcription' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', ),
      'public' => true,
      'has_archive' => true,
      'description' => 'A transcription of a song in the collection',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag' ),
      )
    );
  }

  if ($is_newalexandria) {
    register_post_type( 'belief',
      array(
        'labels' => array(
          'name' => __( 'Beliefs' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Belief',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      )
    );
    register_post_type( 'project',
      array(
        'labels' => array(
          'name' => __( 'Projects' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Project',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'show_in_rest' => true,
      )
    );
    register_post_type( 'member',
      array(
        'labels' => array(
          'name' => __( 'Members' ),
        ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt' ),
      'public' => true,
      'has_archive' => true,
      'description' => 'Member',
      'map_meta_cap' => true,
      'taxonomies' => array( 'category', 'post_tag', 'author' ),
      )
    );
  }

  register_post_type( 'annotation',
    array(
      'labels' => array(
        'name' => __( 'Annotations' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Annotations',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );
  register_post_type( 'lesson_plan',
    array(
      'labels' => array(
        'name' => __( 'Lesson Plans' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Lesson Plan',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );
  register_post_type( 'inscription',
    array(
      'labels' => array(
        'name' => __( 'Inscriptions' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Inscription',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );
}
