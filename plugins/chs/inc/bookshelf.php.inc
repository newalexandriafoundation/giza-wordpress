<?php

// Register the bookshelf REST API endpoint..
add_action( 'rest_api_init', 'chs_register_rest_routes' );
function chs_register_rest_routes() {
    register_rest_route( 'chs/v1', '/bookshelf', [
        'methods'  => 'POST',
        'callback' => function ( $request ) {
          if (is_user_logged_in()) {
            $saved_books = get_user_meta(get_current_user_id(), "saved_books");

            $book_is_already_saved = false;

            if (count($saved_books)) {
              $saved_books = $saved_books[0];

              foreach($saved_books as $key => $book_id) {
                if ($book_id == $request->get_params()['bookID']) {
                  $book_is_already_saved = true;
                  $book_key_to_remove = $key;
                }
              }
            } else {
              $saved_books = array();
            }

            if ($book_is_already_saved) {
              if (count($saved_books)) {
                // remove book from bookshelf
                unset($saved_books[$book_key_to_remove]);
              }

            } else {
              // add book to bookshelf
              $saved_books[] = $request->get_params()['bookID'];
            }

            update_user_meta(get_current_user_id(), "saved_books", $saved_books);
            return true;
          } else {
            return false;
          }
        },
    ] );
}
