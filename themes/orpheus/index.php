<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context          = Timber::context();
$context['homepage'] = new Timber\Post("home");
$context['posts'] = new Timber\PostQuery(array("posts_per_page" => 3, "post_type" => "post"));


$context['posts_art_architecture'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 3, "post_type" => "post", "category_name" => "art-archaeology" ));
$context['posts_epigraphy_papyrology'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 6, "post_type" => "post", "category_name" => "epigraphy-papyrology"));
$context['posts_history'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 9, "post_type" => "post", "category_name" => "history" ));
$context['posts_language_literature'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 12, "post_type" => "post", "category_name" => "language-literature" ));
$context['posts_mythology_religion'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 15, "post_type" => "post", "category_name" => "mythology-religion" ));
$context['posts_philosophy_science'] = new Timber\PostQuery(array("posts_per_page" => 3, "offset" => 18, "post_type" => "post", "category_name" => "philosophy" ));

$context['posts_volumes_9'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-9" ));
$context['posts_volumes_8'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-8" ));
$context['posts_volumes_7'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-7" ));
$context['posts_volumes_6'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-6" ));
$context['posts_volumes_5'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-5" ));
$context['posts_volumes_4'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-4" ));
$context['posts_volumes_3'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-3" ));
$context['posts_volumes_2'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-2" ));
$context['posts_volumes_1'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "tag"=>"volume-1" ));

$context['publications'] = new Timber\PostQuery(array("posts_per_page" => 3, "post_type" => "post", "offset" => 8 ));
$templates        = array( 'index-journal.twig' );


if ($context['is_blog']) {
	$context['posts'] = new Timber\PostQuery(array("posts_per_page" => 6, "post_type" => "post" ));
	$context['publications'] = new Timber\PostQuery(array("posts_per_page" => 9, "post_type" => "book" ));
	$templates        = array( 'index-blog.twig' );
}

if ($context['is_main_site']) {
	//
	$context['posts'] = new Timber\PostQuery(array(
		"posts_per_page" => 10,
		"post_type" => array( "post", "video_post", "event", "curated-article", "feed-item"),
    "tax_query" => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => 'news',
            'include_children' => true,
            'operator' => 'NOT IN'
        )
    ),
	));
	$context['posts_trending'] = new Timber\PostQuery(array(
		"posts_per_page" => 10,
		"post_type" => array( "post", "video_post", "event", "curated-article"),
		'meta_key' => 'wpb_post_views_count',
		'orderby' => 'meta_value_num',
		'order' => 'DESC',
    "tax_query" => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => 'news',
            'include_children' => true,
            'operator' => 'NOT IN'
        )
    ),
	));

	$context['news_posts'] = new Timber\PostQuery(array("posts_per_page" => 3, "post_type" => "post", "category_name" => "news" ));
	$context['programs'] = new Timber\PostQuery(array("posts_per_page" => 3, "post_type" => "post", "category_name" => "programs" ));
	$context['publications'] = new Timber\PostQuery(array("posts_per_page" => 12, "post_type" => "book", "category_name" => "hellenic-studies-series" ));
	$templates        = array( 'index-main.twig' );
}

if ($context['is_milmanparry']) {
	$templates        = array( 'index-mpc.twig' );
	$context['collections'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "collection" ));
	$context['books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book" ));
}

if ($context['is_newalexandria']) {
	$templates        = array( 'index-alex.twig' );
	$context['beliefs'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "belief" ));
	$context['projects'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "project" ));
}

if ($context['is_ilexfoundation']) {
	$context['publications'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book",));
	$templates        = array( 'index-ilex.twig' );
}

if ( is_home() ) {
	array_unshift( $templates, 'front-page.twig', 'home.twig' );
}

$context['inscriptions'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "inscription" ));

Timber::render( $templates, $context );
