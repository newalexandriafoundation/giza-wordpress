<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$post     = new Timber\Term($params['slug']);
$context['post'] = $post;
$context['page_title'] = $post->title;
$context['attachments'] = array();

$context['authors_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "tax_query" => array(array(
	'taxonomy' => 'author',
	'field' => 'slug',
	'terms' => $post->slug,
))));
$context['authors_curated_articles'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "curated-article", "tax_query" => array(array(
	'taxonomy' => 'author',
	'field' => 'slug',
	'terms' => $post->slug,
))));
$context['authors_primary_source'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "primary-source", "tax_query" => array(array(
	'taxonomy' => 'author',
	'field' => 'slug',
	'terms' => $post->slug,
))));

Timber::render( array( 'single-author.twig' ), $context );
