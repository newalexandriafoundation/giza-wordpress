<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$post     = Timber::query_post($params['slug']);
$context['post'] = $post;
$context['page_title'] = $post->title;
$context['attachments'] = array();

$post->meta = get_post_meta($post->ID);
if ($post->meta['attachments']) {
	$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
	$post->meta['attachments'] = $post->meta['attachments']->attachments;

	foreach($post->meta['attachments'] as $attachment) {
		$attachment->url = "/" . $post->category . "/full/" . $attachment->fields->title;

		$file_is_image = false;
		$image_filename_endings = array("jpeg", "jpg", "png", "tiff", "tif");
		foreach ($image_filename_endings as $image_filename_ending) {
			$length = strlen($image_filename_ending);
			if (substr($attachment->fields->title, -$length) === $image_filename_ending){
				$file_is_image = true;
			}
		}
		if ($file_is_image) {
			$context['attachments'][] = $attachment;
		}
	}
}
$context['post']->meta = get_post_meta($post->ID);
$context['tags'] = Timber::get_terms(array( 'taxonomy' => 'post_tag', 'hide_empty' => false, 'number' => 12, 'orderby' => 'count' ));

$context['hellenic_studies_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "hellenic-studies-series" ));
$context['curated_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "curated-books" ));
$context['milman_parry_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "milman-parry-collection-of-oral-literature-series" ));

$context['book_cats'] = get_the_terms($post->ID, 'category');
$context['book_tags'] = get_the_terms($post->ID, 'post_tag' );
$context['book_authors'] = get_the_terms($post->ID, 'author' );

if ($context['book_authors'] && count($context['book_authors'])) {
	$tax_query['relation'] = 'OR' ;
	foreach( $context['book_authors'] as $author) {
		$tax_query[] = array(
			'taxonomy' => 'author',
			'field' => 'slug',
			'terms' => $author->slug,
		);
	}
	$context['authors_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "tax_query" => $tax_query ));
}

$context['categories_books'] = array();
if ($context['book_cats'] && count($context['book_cats'])) {
	foreach( $context['book_cats'] as $cat) {
		$context['categories_books'][] = array(
			'books' => new Timber\PostQuery(array("posts_per_page" => 9, "post_type" => "book", "category_name" => $cat->slug )),
			'list_title' => 'Books from the ' . $cat->name,
			'more_link' => '/category/publications/books-publications/' . $cat->slug,
		);
	}
}

$context['tags_books'] = array();
if ($context['book_tags'] && count($context['book_tags'])) {
	foreach( $context['book_tags'] as $tag) {
		$context['tags_books'][] = array(
			'books' => new Timber\PostQuery(array("posts_per_page" => 9, "post_type" => "book", "tag" => $tag->slug )),
			'list_title' => 'More books about ' . $tag->name,
			'more_link' => '/tag/' . $tag->slug,
		);
	}
}

// check if book is saved by user
$context['book_is_saved'] = false;
foreach( $context['saved_books'] as $book ) {
	if ($context['post']->ID == $book->ID) {
		$context['book_is_saved'] = true;
	}
}


// track post view
wpb_set_post_views($post->ID);

$templates =  array('single-book.twig' );

// custom ilex book templates
if ($context['is_ilexfoundation']) {
	$templates = array( 'single-ilex-book.twig' );
}

// calculate first chapter
$epub = new Timber\Image($post->epub);
$epubParser = new EpubParser($epub->file_loc);
$epubParser->parse();
$toc = $epubParser->getTOC();


$chapters = new Timber\PostQuery(
	array(
	  "post_type" => array( "chapter", ),
		"posts_per_page" => -1,
		"meta_query" => array(
			array(
				"key" => "book_id",
				"value" => $context['post']->ID,
				"compare" => "=",
			),
			'relation' => 'AND',
		),
		"orderby" => "ID",
		'order'   => 'ASC',
	),
);

$context['read_as_webpage_url'] = NULL;

if ($chapters) {
	$context['read_as_webpage_url'] = "/chapter/" . $chapters[0]->post_name;
}

Timber::render( $templates, $context );
