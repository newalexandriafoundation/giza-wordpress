<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-curated-articles.twig' );

$context = Timber::context();


$curated_articles = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "curated-article", 'order' => "ASC", 'orderby' => "title" ));
$context['curated_articles_a_f'] = array();
$context['curated_articles_g_l'] = array();
$context['curated_articles_m_r'] = array();
$context['curated_articles_s_z'] = array();

function lstrip($str, $prefix) {
	if (substr($str, 0, strlen($prefix)) == $prefix) {
	    $str = substr($str, strlen($prefix));
	}

	return $str;
}


foreach ($curated_articles as $curated_article) {
	$curated_article_title_clean = strtolower($curated_article->post_title);
	$curated_article_title_clean = lstrip($curated_article_title_clean, "the ");
	$curated_article_title_clean = lstrip($curated_article_title_clean, "a ");
	$curated_article_title_clean = ltrim($curated_article_title_clean);
	$curated_article_title_clean = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $curated_article_title_clean);
	$first_char_ord = ord($curated_article_title_clean[0]);
	if (ord("a") <= $first_char_ord && $first_char_ord <= ord("f") ) {
		$context['curated_articles_a_f'][] = $curated_article;
	} else if (ord("g") <= $first_char_ord && $first_char_ord <= ord("l") ) {
		$context['curated_articles_g_l'][] = $curated_article;
	} else if (ord("m") <= $first_char_ord && $first_char_ord <= ord("r") ) {
		$context['curated_articles_m_r'][] = $curated_article;
	} else if (ord("s") <= $first_char_ord && $first_char_ord <= ord("z") ) {
		$context['curated_articles_s_z'][] = $curated_article;
	}

	$curated_article->_title_clean = $curated_article_title_clean;
}

usort($context['curated_articles_a_f'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['curated_articles_g_l'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['curated_articles_m_r'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['curated_articles_s_z'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

Timber::render( $templates, $context );
