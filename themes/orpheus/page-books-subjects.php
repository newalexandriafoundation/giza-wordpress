<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-books-terms.twig' );
$context = Timber::context();
$categories = get_terms( 'post_tag', array(
    'hide_empty' => true,
) );

$context['terms_a_f'] = array();
$context['terms_g_l'] = array();
$context['terms_m_r'] = array();
$context['terms_s_z'] = array();



foreach ($categories as $category) {
	$category_title_clean = strtolower($category->name);
	$category_title_clean = ltrim($category_title_clean);
	$category_title_clean = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $category_title_clean);
	$first_char_ord = ord($category_title_clean[0]);
	if (ord("a") <= $first_char_ord && $first_char_ord <= ord("f") ) {
		$context['terms_a_f'][] = $category;
	} else if (ord("g") <= $first_char_ord && $first_char_ord <= ord("l") ) {
		$context['terms_g_l'][] = $category;
	} else if (ord("m") <= $first_char_ord && $first_char_ord <= ord("r") ) {
		$context['terms_m_r'][] = $category;
	} else if (ord("s") <= $first_char_ord && $first_char_ord <= ord("z") ) {
		$context['terms_s_z'][] = $category;
	}

	$category->_title_clean = $category_title_clean;
}

usort($context['terms_a_f'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_g_l'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_m_r'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_s_z'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});


Timber::render( $templates, $context );
