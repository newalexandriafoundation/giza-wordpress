<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-books.twig' );

$context = Timber::context();


$books = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", 'order' => "ASC", 'orderby' => "title" ));
$context['books_a_f'] = array();
$context['books_g_l'] = array();
$context['books_m_r'] = array();
$context['books_s_z'] = array();

function lstrip($str, $prefix) {
	if (substr($str, 0, strlen($prefix)) == $prefix) {
	    $str = substr($str, strlen($prefix));
	}

	return $str;
}


foreach ($books as $book) {
	$book_title_clean = strtolower($book->post_title);
	$book_title_clean = lstrip($book_title_clean, "the ");
	$book_title_clean = lstrip($book_title_clean, "a ");
	$book_title_clean = ltrim($book_title_clean);
	$book_title_clean = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $book_title_clean);
	$first_char_ord = ord($book_title_clean[0]);
	if (ord("a") <= $first_char_ord && $first_char_ord <= ord("f") ) {
		$context['books_a_f'][] = $book;
	} else if (ord("g") <= $first_char_ord && $first_char_ord <= ord("l") ) {
		$context['books_g_l'][] = $book;
	} else if (ord("m") <= $first_char_ord && $first_char_ord <= ord("r") ) {
		$context['books_m_r'][] = $book;
	} else if (ord("s") <= $first_char_ord && $first_char_ord <= ord("z") ) {
		$context['books_s_z'][] = $book;
	}

	$book->_title_clean = $book_title_clean;
}

usort($context['books_a_f'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});


Timber::render( $templates, $context );
