<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );

$context          = Timber::context();
$context['title'] = 'Search results for ' . get_search_query();
$query = array("post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"), "posts_per_page" => 12);

if ($params['page']) {
	$query['paged'] = $params['page'];
}

if ($_GET['category']) {
	$query['category_name'] = $_GET['category'];
}

if ($_GET['s']) {
	$query['_meta_or_keyword'] = $_GET['s'];
	$meta_query_args = array(
		'relation' => 'OR',
		array(
			'key' => 'citation',
			'value' => $_GET['s'],
			'compare' => 'LIKE',
		),
		array(
			'key' => 'collection_description',
			'value' => $_GET['s'],
			'compare' => 'LIKE',
		),
	);
	$query['meta_query'] = $meta_query_args;
}

$context['posts'] = new Timber\PostQuery($query);

Timber::render( $templates, $context );
