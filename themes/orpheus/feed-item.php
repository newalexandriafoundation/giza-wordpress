<?php
/**
 * Redirects route /feed-item to external URL in ACF feed_redirect_url
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

global $params;
$context = Timber::context();

$post = Timber::query_post($params['slug']);
$post->meta = get_post_meta($post->ID);

wp_redirect( $post->meta['feed_redirect_url'][0] ); 
exit;