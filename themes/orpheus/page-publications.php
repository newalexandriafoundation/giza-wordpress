<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-publications.twig', 'archive.twig' );

$context = Timber::context();

$context['title'] = 'Archive';
if ( is_day() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
} elseif ( is_month() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );
} elseif ( is_year() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'Y' );
} elseif ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
} elseif ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} elseif ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

global $params;
$query = array('post_type' => 'book', 'posts_per_page' => 24);

if ($params['page']) {
	$query['paged'] = $params['page'];
}

if ($_GET['category']) {
	$query['category_name'] = $_GET['category'];
}

if ($_GET['tag']) {
	$query['tag'] = $_GET['tag'];
}

$context['tags'] = Timber::get_terms(array( 'taxonomy' => 'post_tag', 'hide_empty' => true, 'number' => 12, 'orderby' => 'count', 'order' => 'DESC' ));

$context['hellenic_studies_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "hellenic-studies-series" ));
$context['curated_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "curated-books" ));
$context['milman_parry_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", "category_name" => "milman-parry-collection-of-oral-literature-series" ));



$posts = Timber::get_posts($query);
$context['posts'] = new Timber\PostQuery($query);

if ($context['is_ilexfoundation']) {
	$timber_post     = new Timber\Post();
	$context['post'] = $timber_post;
	$context['books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book",));
	$templates        = array( 'page-ilex-books.twig' );
}
# $context['post'] = array_shift($posts);
# $context['posts'] = $posts;

// check if book is saved by user
$context['book_is_saved'] = false;
foreach( $context['saved_books'] as $book ) {
	if (172775 == $book->ID) {
		$context['book_is_saved'] = true;
	}
}

Timber::render( $templates, $context );
