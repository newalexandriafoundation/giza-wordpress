<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$expressionsOfWoe = array(
    'ἰὼ ἰώ',
    'αἶ, αἶ',
    'οἴμοι μοι',
    'φεῦ φεῦ',
    'ἰώ μοί μοι',
    'ὦ Ζεῦ',
    'βοᾷ βοᾷ',
    'αἰαῖ αἰαῖ',
    'ἔα ἔα',
    'ὀττοτοτοτοτοῖ',
    'ἄλγος ἄλγος βοᾷς',
    'ἐλελεῦ',
    'μὴ γένοιτο',
    'οὐαί',
  );
$context['expression_of_woe'] = $expressionsOfWoe[array_rand($expressionsOfWoe)];

$books = new Timber\PostQuery(
	array(
	  "post_type" => array( "book", ),
		"posts_per_page"   => -1,
	)
);

// delete all chapters
$allposts= get_posts( array('post_type'=>'chapter','numberposts'=>-1) );
foreach ($allposts as $eachpost) {
	wp_delete_post( $eachpost->ID, true );
}


foreach ($books as $book) {
	$epub = new Timber\Image($book->epub);

	try {
		$epubParser = new EpubParser($epub->file_loc);
		$epubParser->parse();
	} catch (Exception $e) {
		error_log("Exception $e");
		continue;
	}

	$toc = $epubParser->getTOC();
	$context['toc'] = $toc;

	foreach($toc as $i => $chapter) {
    $post_data = array(
        'post_title' => $chapter['name'],
        'post_status' => 'publish',
        'post_type' => 'chapter',
				'meta_input' => array(
					'book_id' => $book->ID,
				),
    );

		try {
      $post_data['post_content'] = $epubParser->getChapter('chapter_' . $i);
		} catch (Exception $e) {
			try {
	      $post_data['post_content'] = $epubParser->getChapter('id' . $i);
			} catch (Exception $e) {
	      $post_data['post_content'] = '';
			}
		}

    wp_insert_post( $post_data );
	}

	unset($epub);
	unset($epubParser);
	unset($toc);
}

Timber::render( array( '404.twig' ), $context );
