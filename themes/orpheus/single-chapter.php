<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$post     = Timber::query_post($params['id']);
$context['post'] = $post;
$context['page_title'] = $post->title;
$context['attachments'] = array();

$context['post']->meta = get_post_meta($post->ID);

// track post view
wpb_set_post_views($post->ID);
if (count($context['post']->meta['book_id'])) {
	$context['post']->meta['book_id'] = intval($context['post']->meta['book_id'][0]);
}

$context['chapters'] = new Timber\PostQuery(
	array(
	  "post_type" => array( "chapter", ),
		"posts_per_page" => -1,
		"meta_query" => array(
			array(
				"key" => "book_id",
				"value" => $context['post']->meta['book_id'],
				"compare" => "=",
			),
			'relation' => 'AND',
		),
		"orderby" => "ID",
		'order'   => 'ASC',
	),
);

$context['book'] = new Timber\Post($context['post']->meta['book_id']);

Timber::render( array( 'single-chapter.twig' ), $context );
