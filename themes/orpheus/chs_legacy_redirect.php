
<?php
/**
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */


// Redirect links like this
// CHS/article/display/7125.manuela-pellegrino-greek-language-italian-landscape-griko-and-the-re-storying-of-a-linguistic-minority
//  // to internal wordpress links

// default redirect to 404
$url = '/404/' . $wp->request;
$request_url = $wp->request;
$request_url = str_replace("CHS/article/display/", "", $request_url);
$params = explode(".", $request_url);
$legacy_id = NULL;

if (count($params)) {
  $legacy_id = intval($params[0]);
}

if ($legacy_id) {
  $query = new WP_Query(array(
    'post_type'  => array('post', 'book', 'curated-article', 'event', 'primary-source'),
    'meta_query' => array(
      array(
        'key' => 'legacy_post_id',
        'value' => $legacy_id,
        'compare' => '=',
      )
    )
  ));
  foreach ($query->get_posts() as $post) {
    $url = '/' . $post->post_type . '/' . $post->post_name;
    error_log(var_export($url, true));
  }
}


wp_redirect( $url );
exit;
