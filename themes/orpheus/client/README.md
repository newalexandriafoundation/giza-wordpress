# Alexandria React Frontend

[![pipeline status](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/badges/develop/pipeline.svg)](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/commits/develop)
[![coverage report](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/badges/develop/coverage.svg)](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/commits/develop)

The project consists of 2 applications:

1.  React / Redux frontend application based on [`create-react-app`](https://github.com/facebookincubator/create-react-app) in this [repository](/packages/orpheus)

2.  GraphQL API (Node.js / Express) here: http://gitlab.archimedes.digital/archimedes/orpheus-api/tree/master

## Requirements

In order to develop with this application, you will need the following software installed on your development environment:

* Node.js@10.x.x and npm@6

You can install Node.js and npm through your OS's package manager (e.g., `brew install nodejs`) or by visiting the Node.js [download](https://nodejs.org/en/) page.

Make sure `node` and `npm` work before proceeding.

```sh
$ node -v
10.15.0

$ npm -v
6.6.0
```


## Starting the project

### Modify hosts file on your local machine

On your local machine, add the following to your hosts file (on OSX at `/etc/hosts`):

```
127.0.0.1   orpheus.local
127.0.0.1   [project_name].orpheus.local
```

Repeat adding projects with their project name to the hosts file for the projects you'd like to create and work on locally.


### Start the React frontend

To start the project you need to follow these steps:

1.  Clone this repository

2.  Install dependencies

  ```sh
  $ npm i
  ```

3.  Set up environment variables:

  Configure these environment variables as necessary for your development environment so that the frontned knows which URL to access the GraphQL API backend at and which other routes to use for the authentication information.

  Environment variables for the frontend:

  ```sh
  $ cp .env.example .env
  # or copy these values into your own .env
  REACT_APP_GRAPHQL_SERVER=http://api.orpheus.local:3001
  REACT_APP_GRAPHQL_URI=graphql
  REACT_APP_SERVER=http://api.orpheus.local:3001
  REACT_APP_LOGIN_URI=auth/login
  REACT_APP_LOGOUT_URI=auth/logout
  REACT_APP_REGISTER_URI=auth/register
  REACT_APP_VERIFY_TOKEN_URI=auth/verify-token
  REACT_APP_COOKIE_DOMAIN=mindthegap.orpheus.local
  REACT_APP_BUCKET_URL=https://s3.amazonaws.com/iiif-orpheus
  REACT_APP_FACBOOK_CLIENT_ID=client_id
  REACT_APP_GOOGLE_CLIENT_ID=client_id
  REACT_APP_TWITTER_CLIENT_ID=client_id
  REACT_APP_GOOGLE_MAPS_API_KEY=mapkey
  ```

  Variables for using local api:
  ```sh
  REACT_APP_GRAPHQL_SERVER=http://api.orpheus.local:3001
  REACT_APP_SERVER=http://api.orpheus.local:3001
  REACT_APP_COOKIE_DOMAIN=.orpheus.local
  ```

  Variables for using staging api:
  ```sh
  REACT_APP_GRAPHQL_SERVER=http://api.staging.orphe.us
  REACT_APP_SERVER=http://api.staging.orphe.us
  REACT_APP_COOKIE_DOMAIN=.staging.orpheus.local
  ```

  Variables for using production api:
  ```sh
  REACT_APP_GRAPHQL_SERVER=http://api.orphe.us
  REACT_APP_SERVER=http://api.orphe.us
  REACT_APP_COOKIE_DOMAIN=.orpheus.local
  ```

4.  Start the engines.

  After configuring the environment variables and installing packages, you can start the application with
  ```sh
  $ npm start
  ```
**!IMPORTANT** do NOT commit `.env` files to the repository. These should be used for __personal configuration__ and __secret values__.

## Starting Storybook (component library)

Follow the instructions above, but instead of `npm start`, run `npm run storybook`.

# FAQ

## Why do the above commands use `npm`? I thought `yarn` was the way to go.

Yarn has several [open](https://github.com/yarnpkg/yarn/issues/5235) [issues](https://github.com/yarnpkg/yarn/issues/5709) related to CHS' usecase that make `npm` a safer bet.

# Troubleshooting

This section includes some common errors and potential solutions. It will be updated as more pain points are uncovered.

## I'm getting errors about missing packages.

First try running `npm i` --- it could be that a few packages needed to be updated. (It might also be advisable to `rm -r[f] ./node_modules`.)

If the failure happened in the building stage, read on in this [#Troubleshooting](#troubleshooting) section.

## JavaScript won't build.

If `webpack` (the JavaScript build tool used by CHS) fails, first see if it gives you specific instructions to fix the problem. Follow those instructions first.

If `webpack` fails to start the development server, make sure that nothing else is running on the port it's trying to claim.

If `webpack` reports an error related to a specific line in a specific file, try to fix that line. E.g., if `webpack` says, `"./src/components/Iliad/Iliad.js:61: Homer was undefined"`, make sure you've declared the variable `Homer` (or imported the `Homer` package, wherever that might be) somewhere before line `61` in the file at `./src/components/Iliad/Iliad.js`.

If JavaScript still fails to build, capture as much of the stacktrace as possible, paste it into [Slack](https://archimedes-digital.slack.com) and @-mention `@charles`.

## CSS won't build.

CSS is still finicky with modern build tools. CHS uses SCSS for writing its styles, which the build tool compiles to CSS before it's loaded into the component(s) where it's needed.

CHS' styles live in their own repository at [archimedes/orpheus-styles](https://gitlab.archimedes.digital/archimedes/orpheus-styles).

If you're getting CSS "File Not Found" errors, try running `npm link` to make sure that symlinks exist where they're supposed to exist. (They can break when `node_modules` is updated.) You might need to kick off the CSS build process with an `npm run build` in your local `orpheus-styles` repo.

If you're getting SCSS undefined variable errors, make sure that you're importing the correct CSS file. The error will usually give you a hint about where to look.

If CSS still fails to build, capture as much of the stacktrace as possible, paste it into [Slack](https://archimedes-digital.slack.com) and @-mention `@charles`.

## Storybook won't start

[`create-react-app@v2`](https://github.com/facebook/create-react-app) brought with it a [whole](https://github.com/storybooks/storybook/issues/3806) [host](https://github.com/storybooks/storybook/issues/3177) of [issues](https://github.com/storybooks/storybook/issues/4306) for the [Storybook](https://storybook.js.org/) team. (A summary of much of the discussion can be found in this [issue](https://github.com/storybooks/storybook/issues/4995) (open as of January 22, 2019).) One of those issues causes the `storybook` command to stall, despite the package having built successfully.

This is one of the reasons that we needed to `eject` from Create-React-App, so that we could downgrade `sass-loader` from `v7.1.0` to `v6.0.7`. Storybook _should_ work with the new-old `sass-loader`, but please be aware that changes like this can occur from time to time. We'll do our best to catch them, but such is the nature of Node.js development in 2019.
