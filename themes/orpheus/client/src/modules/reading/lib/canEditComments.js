export default function canEditComments() {
	return window.USER_IS_ADMIN || window.USER_IS_EDITOR || window.USER_IS_COMMENTER;
}