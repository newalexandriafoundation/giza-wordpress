const getCurrentPathname = () => {
	const pathnameArray = location.pathname.split('.');
	return pathnameArray[0];
};


export default getCurrentPathname;
