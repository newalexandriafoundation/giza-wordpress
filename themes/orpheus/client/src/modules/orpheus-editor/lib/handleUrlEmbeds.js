import isValidUrl from './isValidUrl';
import insertNewBlock from './insertNewBlock';

const handleUrlEmbeds = ({ editorState, setEditorState }) => {
	const currentContent = editorState.getCurrentContent();
	const currentBlockKey = editorState.getSelection().getStartKey();
	const currentContentBlock = currentContent.getBlockForKey(currentBlockKey);
	const currentContentBlockText = currentContentBlock.getText();


	if (isValidUrl(currentContentBlockText)) {
		const options = {
			inputUrl: currentContentBlockText,
		};

		let newEditorState;
		if (currentContentBlockText.indexOf('youtube') >= 0) {
			newEditorState = insertNewBlock(editorState, 'video', options);
		} else {
			newEditorState = insertNewBlock(editorState, 'embed', options);
		}

		setEditorState(newEditorState);
	}
};

export default handleUrlEmbeds;
