import {
	ContentBlock,
	ContentState,
	EditorState,
	genKey
} from 'draft-js';

import {
	listTypeCandidate,
	makeListItem,
} from './listHandlers';

const makeContentBlock = text => new ContentBlock({
	data: {},
	depth: 0,
	key: genKey(),
	text,
	type: 'unstyled',
});

describe('listHandlers', () => {
	describe('listTypeCandidate()', () => {
		it('returns "unordered-list-item" if it starts with -', () => {
			const block = makeContentBlock('- ');

			expect(listTypeCandidate(block)).toEqual('unordered-list-item');
		});

		it('returns "unordered-list-item" if it starts with *', () => {
			const block = makeContentBlock('* ');

			expect(listTypeCandidate(block)).toEqual('unordered-list-item');
		});

		it('returns "ordered-list-item" if it starts with 1.', () => {
			const block = makeContentBlock('1. ');

			expect(listTypeCandidate(block)).toEqual('ordered-list-item');
		});
	});

	describe('makeListItem()', () => {
		it('makes the current block a list item of the given type', () => {
			let block = makeContentBlock('- item');
			let editorState = EditorState.createWithContent(
				ContentState.createFromBlockArray([block])
			);
			editorState = makeListItem(editorState, block, 'unordered-list-item');
			block = editorState.getCurrentContent().getLastBlock();

			expect(block.getType()).toEqual('unordered-list-item');
			expect(block.getText()).toEqual('item');
		});
	})
});
