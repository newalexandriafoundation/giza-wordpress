import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

global.noop = () => {};
global.URL = {
	createObjectURL: global.noop,
};
