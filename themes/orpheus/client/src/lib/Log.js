/**
 * @prettier
 */

const DEFAULT_LEVELS = {
	error: '#b23b41', // red
	info: '#3b7db2', // blue
	log: '#3b7db2', // blue
	warn: '#e3db3c', // yellow
};

class Log {
	constructor(levels = DEFAULT_LEVELS) {
		this.levels = levels;

		for (let level in levels) {
			this[level] = msg =>
				console[level](['%c', msg].join(' '), `color: ${levels[level]}`);
		}
	}
}

export { Log };
export default new Log();
