export function dateShortMonthFormat(date) {
	const shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	date = new Date(date);
	return `${date.getDate()} ${shortMonths[date.getMonth()]} ${date.getFullYear()}`;
}
