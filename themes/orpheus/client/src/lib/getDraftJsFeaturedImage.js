import shortid from 'shortid';
import { convertFromRaw } from 'draft-js';

/**
 * getDraftJsFeaturedImage
 * @param {string} bodyContent - json from draftjs state stored as string in db
 * @returns {Object} featured image data
 */
const getDraftJsFeaturedImage = ({ bodyContent }) => {
	let featuredImage = {
		_id: shortid.generate(),
		name: '',
		type: '',
		path: '',
		thumbPath: '',
		label: '',
	};
	let featuredImageSet = false;

	let contentState;
	// get content state from raw json stored in db
	try {
		contentState = convertFromRaw(bodyContent);
	} catch (e) {
		// console.log(e);
	}

	if (!contentState) {
		return null;
	}

	// get block map from content state and extract text from each block
	const blockMap = contentState.getBlockMap();
	blockMap.mapEntries((entry) => {
		if (!featuredImageSet) {
			if (entry[1].getType() === 'image') {
				const block = entry[1];
				const blockJSON = block.toJSON();
				featuredImage.path = blockJSON.data.url;
				featuredImage.label = blockJSON.data.text;
				const splitURL = blockJSON.data.url.split('/');
				featuredImage.name = splitURL[splitURL.length - 1];
				featuredImageSet = true;
			} else if (entry[1].getType() === 'embed') {
				const block = entry[1];
				const blockJSON = block.toJSON();

				if (
					blockJSON.data.embed_data
					&& blockJSON.data.embed_data.images
					&& blockJSON.data.embed_data.images.length
				) {
					const url = blockJSON.data.embed_data.images[0].url;
					featuredImage.path = url;
					featuredImage.label = url;
					const splitURL = url.split('/');
					featuredImage.name = splitURL[splitURL.length - 1];
					featuredImageSet = true;
				}

			}
		}
	});

	return featuredImage;
};

export default getDraftJsFeaturedImage;
