const monthNames = [ "January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December" ];

const dateFormat = dateValue => {
  let date = new Date(dateValue);
  return `${monthNames[date.getMonth()]} ${date.getFullYear()}`;
}

export default dateFormat;
