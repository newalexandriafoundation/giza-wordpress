/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { branch, renderComponent } from 'recompose';
import { CircularProgress } from '@material-ui/core';

// inspiration: https://www.apollographql.com/docs/react/recipes/recompose#loading-status
const Loading = props => (
	<div style={props.style}>
		<CircularProgress />
	</div>
);

Loading.defaultProps = {
	style: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
	},
};

Loading.propTypes = {
	style: PropTypes.object,
};

export default (propName = 'data', Placeholder = Loading) =>
	branch(
		// props[propName].loading is set by Apollo
		props => props[propName] && props[propName].loading,
		renderComponent(Placeholder)
	);
