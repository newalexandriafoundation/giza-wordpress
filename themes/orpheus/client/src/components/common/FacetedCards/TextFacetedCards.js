import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import Header from '../../navigation/Header';

import Footer from '../../navigation/Footer';
import List from '../lists/List';
import CustomTheme from '../../../lib/muiTheme';
import Filters from '../Filters';
import Collections from '../../../modules/texts/graphql/texts';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import TextListContainer from '../../../modules/texts/containers/TextsListContainer';

import ArchiveFiltersContainer from '../../../modules/texts/containers/TextsFiltersContainer';
import qs from 'qs-lite';

import { MuiThemeProvider } from '@material-ui/core';


class FacetedCards extends React.Component {
	constructor(props) {
		super(props);
	}
	

	render () {
		let filterLookup = {};
		let items = [];
		let filteredItems = [];
		
		const query = qs.parse(window.location.search.replace('?', ''));

		filterLookup.collection =  {
			name: 'Collection',
			type: 'text',
			values: []
		}
		filterLookup.textGroup =  {
			name: 'Author',
			type: 'text',
			values: []
		}
		filterLookup.structure =  {
			name: 'Structure',
			type: 'text',
			values: []
		}
		filterLookup.work_type =  {
			name: 'Work Type',
			type: 'text',
			values: []
		}
		filterLookup.language =  {
			name: 'Language',
			type: 'text',
			values: []
		}
		Collections.forEach((col) => {
			filterLookup.collection.values.push(col.title)
			col.textGroups.forEach((tg) => {
				filterLookup.textGroup.values.push(tg.title);
				tg.works.forEach((text) => {
					text.collection = col.title;
					text.textGroup = tg.title;
					text.type='text';
					
					if (text.language && text.language.title && !filterLookup.language.values.includes(text.language.title)) {
						filterLookup.language.values.push(text.language.title);
					   }
					   
					if (text.work_type && text.work_type.length > 0 && !filterLookup.work_type.values.includes(text.work_type)) {
						filterLookup.work_type.values.push(text.work_type);
				   }

				   if (text.structure && text.structure.length > 0 && !filterLookup.structure.values.includes(text.structure)) {
						filterLookup.structure.values.push(text.structure);
					}
					items.push(text)
					let matchesFilters = true;
					if (query['Collection'] && text.collection !== query['Collection']) {
						matchesFilters = false;
					}
					if (query['Author'] && text.textGroup !== query['Author']) {
						matchesFilters = false;
					}
					if (query['Structure'] && text.structure !== query['Structure']) {
						matchesFilters = false;
					}
					if (query['Work Type'] && text.work_type !== query['Work Type']) {
						matchesFilters = false;
					}
					if (query['Language'] && text.language && text.language.title !== query['Language']) {
						matchesFilters = false;
					}
					if (matchesFilters) {
						filteredItems.push(text)
					}
				})
			})
		})

		const filters = Object.keys(filterLookup).map((filter_name) => {
			return filterLookup[filter_name]
		})

		const _classes = this.props.classes || [];

		_classes.push('facetedCards');

		if (this.props.loading) {
			_classes.push('-loading');
		}

		return (
			<div className={_classes.join(' ')}>
                <h5>Texts <NavigateNextIcon style={{position: 'relative', top: '5px'}}/></h5>
                <h2>All Texts</h2>
				<div className="facetedCardsContent">
					<Sticky
						className="facetedCardsContentFilters"
						activeClass="-sticky"
						bottomBoundary=".sticky-reactnode-boundary"
						enabled
					>
						<div className="facetedCardsContentFiltersInner">
							{this.props.loading ? (
								<ArchiveFiltersContainer loading />
							) : (
								<ArchiveFiltersContainer filters={filters} items={filteredItems}/>
							)}
						</div>
					</Sticky>
					<div className="facetedCardsContentCards">
						{this.props.items ?
							<List
								loading={this.props.loading}
								selectable={this.props.selectable}
								items={this.props.items}
								isAdmin={this.props.isAdmin}
							/>
						:
							<TextListContainer
								loading={this.props.loading}
								selectable={this.props.selectable}
								isAdmin={this.props.isAdmin}
								items={filteredItems}
							/>
						}
					</div>
				</div>
			</div>
		);
	}
}

FacetedCards.propTypes = {
	theme: PropTypes.string,
	selectable: PropTypes.bool,
	isAdmin: PropTypes.bool,

}

FacetedCards.defaultProps = {
	theme: '',
};


export default FacetedCards;
