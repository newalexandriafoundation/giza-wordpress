import React from 'react';
import { shallow } from 'enzyme';

// component
import Profile from './Profile';

describe('Profile', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Profile />);
		expect(wrapper).toBeDefined();
	});
});
