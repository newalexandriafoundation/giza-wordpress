import React from 'react';
import { shallow } from 'enzyme';

// component
import HorizontalList from './HorizontalList';

describe('HorizontalList', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<HorizontalList />);
		expect(wrapper).toBeDefined();
	});
});
