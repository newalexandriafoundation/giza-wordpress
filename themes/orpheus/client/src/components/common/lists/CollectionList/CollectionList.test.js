import React from 'react';
import { shallow } from 'enzyme';

// component
import CollectionList from './CollectionList';

describe('CollectionList', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<CollectionList />);
		expect(wrapper).toBeDefined();
	});
});
