import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

// component
import List from './List';
import configureStore from '../../../../store/configureStore';

describe('List', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<Provider store={configureStore()}>
				<List />
			</Provider>
		);
		expect(wrapper).toBeDefined();
	});
});
