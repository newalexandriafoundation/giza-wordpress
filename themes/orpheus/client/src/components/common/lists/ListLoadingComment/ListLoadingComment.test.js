import React from 'react';
import { shallow } from 'enzyme';

// component
import ListLoadingComment from './ListLoadingComment';

describe('ListLoadingComment', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ListLoadingComment />);
		expect(wrapper).toBeDefined();
	});
});
