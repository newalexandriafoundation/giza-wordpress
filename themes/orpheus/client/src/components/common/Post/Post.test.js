import React from 'react';
import { shallow } from 'enzyme';

// component
import Post from './Post';

describe('Post', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Post to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
