import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import ItemEditorUploader from '../../../modules/dashboard/components/ItemEditorUploader';
import PageMeta from '../../../components/common/PageMeta';
import FacetedCards from '../FacetedCards';



const Dashboard = (props) => {

	const {
		classes,
		empty,
		loading,
		uploading,
		uploadProgress,
		anonymous
	} = props;

	const _classes = classnames(classes, 'dashboard', {
		'-empty': empty,
		'-loading': loading,
		'-uploading': uploading
	});

	if (loading) {
		return (
			<div className={_classes}>
				<FacetedCards
					isAdmin={true}
					loading
				/>
			</div>
		);
	}

	return (
		<div className={_classes}>
			<PageMeta
				pageTitle={props.archiveTitle ? `Dashboard — ${props.archiveTitle}`: 'Dashboard'}
			/>
			{ uploading &&
				<div className="dashboardUploading">
					<Paper className="dashboardUploadingPaper" elevation={3}>
						<Typography variant="body1" component="p">
							Uploading files...
						</Typography>
						<LinearProgress
							variant="determinate"
							value={uploadProgress}
						/>
					</Paper>
				</div>
			}
			{(empty) ?
				<React.Fragment>
					<div className="dashboardEmpty">
						{!props.files || !props.files.length ?
							<React.Fragment>
								<div className="dashboardEmptyInstructions">
								</div>
							</React.Fragment>
							: ''}
					</div>
				</React.Fragment>
				:
				<div>
					<FacetedCards
						isAdmin={props.isAdmin}
						selectable
					/>
					{props.anonymous &&
						<div className="dashboardAnonymous">
							<div className="dashboardAnonymousContent">
							</div>
						</div>
					}
				</div>
			}
		</div>
	);
};

Dashboard.propTypes = {
	empty: PropTypes.bool,
	loading: PropTypes.bool,
	uploading: PropTypes.bool,
	anonymous: PropTypes.bool,
	isAdmin: PropTypes.bool,
	uploadProgress: PropTypes.number,
};

Dashboard.defaultProps = {
	uploadProgress: 0,
	isAdmin: false,
};

export default Dashboard;
