import React from 'react';
import { shallow } from 'enzyme';

// component
import CardLoadingComment from './CardLoadingComment';

describe('CardLoadingComment', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<CardLoadingComment to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
