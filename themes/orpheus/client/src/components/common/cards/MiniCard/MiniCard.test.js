import React from 'react';
import { shallow } from 'enzyme';

// component
import MiniCard from './MiniCard';

describe('MiniCard', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<MiniCard to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
