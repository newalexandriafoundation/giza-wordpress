import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import prune from 'underscore.string/prune';

import { mockFiles } from '../../../../mocks/files';




const GroupCard = ({
	type, _id, slug, loading, title, description, itemsCount, className, files
}) => {
	let itemUrl = `/${type}/${_id}/${slug}`;

	const _classes = ['groupCard'];

	if (loading) {
		_classes.push('-loading');
	}

	if (className) {
		_classes.push(className);
	}

	return (
		<Paper
			elevation={2}
			className={_classes.join(' ')}
		>
			<Link
				to={itemUrl}
			>
				{files ?
					<div className="groupCardFiles">
						{files.map(file => {
							const fileType = file.type || '';
							const isImage = fileType.slice(0, fileType.indexOf('/')) === 'image';
							if (!isImage) {
								return null;
							}

							return (
								<div
									className="groupCardFile"
									style={{
										backgroundImage: `url('//iiif.orphe.us/${file.name}/full/90,/0/default.jpg')`,
									}}
									key={file._id}
								/>
							);
						})}
					</div>
				: ''}
				<div className="groupCardInner">
					<Typography
						className="groupCardTitle"
						variant="body1"
					>
						{title}
					</Typography>
					<Typography
						className="groupCardDescription"
						variant="caption"
					>
						{prune(description, 100, '')}
					</Typography>
					<Typography
						className="groupCardMeta"
						variant="caption"
					>
						{itemsCount} items
					</Typography>
				</div>
			</Link>
		</Paper>
	);
};

GroupCard.propTypes = {
	_id: PropTypes.string,
	title: PropTypes.string,
	slug: PropTypes.string,
	itemsCount: PropTypes.number,
	files: PropTypes.array,
	description: PropTypes.string,
	author: PropTypes.string,
	type: PropTypes.string,
	loading: PropTypes.bool,
	compact: PropTypes.bool,
};

GroupCard.defaultProps = {
	type: 'items',
};

export default GroupCard;
