import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';

const SearchInput = props => (
	<div className="searchInput">
		<InputBase placeholder="Search" />
		<IconButton aria-label="Search">
			<SearchIcon />
		</IconButton>
	</div>
);

export default SearchInput;
