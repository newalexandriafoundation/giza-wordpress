import React from 'react';
import { shallow } from 'enzyme';

// component
import Filter from './Filter';

describe('Filter', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Filter to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
