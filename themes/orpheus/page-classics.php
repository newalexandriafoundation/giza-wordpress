<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/templates/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;

function title_filter( $where, &$wp_query ){
    global $wpdb;
    if ( $search_term = $wp_query->get( 'search_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
    }
    return $where;
}

add_filter( 'posts_where', 'title_filter', 10, 2 );
$context['posts_classics_17'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@17" ));
$context['posts_classics_16'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@16" ));
$context['posts_classics_15'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@15" ));
$context['posts_classics_14'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@14" ));
$context['posts_classics_13'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@13" ));
$context['posts_classics_12'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@12" ));
$context['posts_classics_11'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@11" ));
$context['posts_classics_10'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@10" ));
$context['posts_classics_9'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@9" ));
$context['posts_classics_8'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@8" ));
$context['posts_classics_7'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@7" ));
$context['posts_classics_6'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@6" ));
$context['posts_classics_5'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@5:" ));
$context['posts_classics_4'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@4:" ));
$context['posts_classics_3'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@3:" ));
$context['posts_classics_2'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@2:" ));
$context['posts_classics_1'] = new Timber\PostQuery(array("posts_per_page" => -1, "offset" => 0, "post_type" => "post", "category_name" => "classics", 'search_title' => "Classics@1:" ));
remove_filter( 'posts_where', 'title_filter', 10, 2 );

Timber::render( array('page-classics.twig'), $context );
